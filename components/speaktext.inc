<?php

/**
 * @file
 * Webform module speaktext component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_speaktext() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'extra' => array(
      'format' => FILTER_FORMAT_DEFAULT,
      'private' => FALSE,
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_speaktext($component) {
  $form = array();
  $form['speaktext']['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Value'),
    '#default_value' => $component['value'],
    '#description' => t('Read this text to the VoIP webform participant.'),
    '#weight' => -1,
    '#parents' => array('value'),
  );
  // Add the filter form.
  $form['speaktext']['format'] = filter_form($component['extra']['format'], 0, array('extra', 'format'));

  return $form;
}


/**
 * Implements _webform_render_component().
 */
function _webform_render_speaktext($component, $value = NULL, $filter = TRUE) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $element = array(
    '#type' => 'speaktext',
    '#title' => $filter ? NULL : $component['name'],
    '#weight' => $component['weight'],
    '#speaktext' => $filter ? _webform_filter_values(check_markup($component['value'], $component['extra']['format'], FALSE), $node, NULL, NULL, FALSE) : $component['value'],
    '#format' => $component['extra']['format'],
    '#theme_wrappers' => array('webform_element_wrapper'),
    '#post_render' => array('webform_element_wrapper'),
    '#translatable' => array('title', 'speaktext'),
  );

  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_speaktext($component, $value, $format = 'html') {
  return array();
}
