<?php

/**
 * Declare input handlers.
 *
 * Callbacks should take ($script, $type, $element)
 *
 * @see voipwebform_input_handler
 */
function hook_voipwebform_input_handlers() {
  $ret = array();

  $ret['somewidget']['callback'] = 'mymodule_voip_input_handler';

  return $ret;
}

function mymodule_voip_input_handler($script, $type, $element) {
  switch ($type) {
    case 'somewidget':
      $script->addSay('This is a widget!');
      $script->addGetInput('Enter a number!');
      // You must set 'response' so we can add it to the webform results.
      $script->addSet('response', '%input_digits');
  }
}
